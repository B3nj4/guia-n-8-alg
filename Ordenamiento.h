#ifndef ORDENAMIENTO_H
#define ORDENAMIENTO_H

//Librerias
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

// Clase ordenamiento
class Ordenamiento {
    private:
        
    public:
        //Constructor
        Ordenamiento();
        
        //Impresiones
        void imprimir_vector(int *vector, int n);

        // Selección
        void seleccion(int *vector, int n);

        // Quicksort (Vector = arreglo A y n tamaño ingresado por teclado)
        void quicksort(int *vector, int n); 

};
#endif