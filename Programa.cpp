//Librerias
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <unistd.h>
#include <time.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

#include "Ordenamiento.h"

void imprime_terminal(double secs1, double secs2){
    cout << endl;
    cout << "-----------------------------------------" << endl;
    cout << "Método           | Tiempo                 " << endl;
    cout << "-----------------------------------------" << endl;
    cout << "Seleccion        | " << secs1*1000.0 << " milisegundos         " << endl;
    cout << "Quicksort        | " << secs2*1000.0 << " milisegundos         " << endl;
    cout << "-----------------------------------------" << endl;
    cout << endl;
}

int main(int argc, char **argv) {
    
    // Variables
    int n, opc;
    Ordenamiento ordenamiento;
    clock_t t1, t2, t3, t4;
    double secs1, secs2;
    
    // Convierte string a entero y valida cantidad de parámetros mínimos.
    n = atoi(argv[1]);
    opc = atoi(argv[2]);
    if (n <= 2) {
        cout << "Error: Ingrese un dato correcto (numero positivo mayor o igual a 2)" << endl;
        return -1;
    }
    if (opc < 0 || opc > 2){
        cout << "Error: Ingrese un dato correcto (1 si quiere mostrar vectores y 2 para lo contrario)" << endl;
        return -1;
    }

    // Intancia vector A con numeros random
    srand((unsigned) time(0));
    int randomNumber;
    int A[n];
    int A_original_quick[n], A_original_seleccion[n];
    for (int index = 0; index < n; index++) {
        randomNumber = (rand() % 35) + 1;
        A[index] = randomNumber;
        A_original_quick[index] = A[index];
        A_original_seleccion[index] = A[index];
    }
    
    // <Uso de quicksort>
    if (opc ==1){
        cout << endl <<"<Uso de Quicksort> "<<endl;
        /*Imprime el vector original (desorden)*/
        cout << "Vector Original: ";
        ordenamiento.imprimir_vector(A_original_quick, n);
    }
    
    /*Aplica el ordenamiento ademas de contar (t1 y t2) el tiempo de ejecucion*/
    t1 = clock();
    ordenamiento.quicksort(A_original_quick, n);
    t2 = clock();
    
    if(opc == 1){
        /*Imprime el vector ordenado con el metodo respectivo*/
        cout << "Vector Reordenado: ";
        ordenamiento.imprimir_vector(A_original_quick, n);
    }
    secs1 = (double)(t2 - t1) / CLOCKS_PER_SEC;
    
    
    // <Uso de seleccion>
    if (opc == 1){
        cout << endl << "<Uso de Seleccion> "<<endl;
        /*Imprime el vector original (desorden)*/
        cout << "Vector Original: ";
        ordenamiento.imprimir_vector(A_original_seleccion, n);
    }
    
    /*Aplica el ordenamiento ademas de contar (t3 y t4) el tiempo de ejecucion */
    t3 = clock();
    ordenamiento.seleccion(A_original_seleccion, n);
    t4 = clock();
    
    if (opc == 1){
        /*Imprime el vector ordenado con el metodo respectivo*/
        cout << "Vector Reordenado: ";
        ordenamiento.imprimir_vector(A_original_seleccion, n); 
    }
    secs2 = (double)(t4 - t3) / CLOCKS_PER_SEC;
    
    // Imprime la informacion del tiempo de demora de los ordenamientos*/
    imprime_terminal(secs1, secs2);

    return 0;
}