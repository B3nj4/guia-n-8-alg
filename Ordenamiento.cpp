#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cstring>
using namespace std;

//Librerias
#include "Ordenamiento.h"

// Constructor
Ordenamiento::Ordenamiento() {}

void Ordenamiento::imprimir_vector(int *vector, int n){
    // REcorre vector para imprimirlo uno por uno
    for(int i=0; i<n; i++){
        cout << vector[i] << "  ";
    }
    cout << endl;
}

void Ordenamiento::seleccion(int *vector, int n){
    // Variables
    int MENOR, AUX;
    
    // Algoritmo para reordenar el vector mediante seleccion
    for(int i=0; i<=n-2; i++) {
        MENOR = vector[i];
        AUX = i;
        
        for (int j=i+1; j<=n-1; j++) {
            if (vector[j] < MENOR) {
                MENOR = vector[j];
                AUX = j;
            }
        }
        
        // Se actualizan los datos para seguir con otro elemento
        vector[AUX] = vector[i];
        vector[i] = MENOR;
    }
}

void Ordenamiento::quicksort(int *vector, int n){
    // Variables
    int TOPE, INI, FIN, POS;
    int PILAMENOR[100];
    int PILAMAYOR[100];
    int IZQ, DER, AUX, BAND; 
    
    // INstanciacion de variables
    TOPE = 0;
    PILAMENOR[TOPE] = 0;
    PILAMAYOR[TOPE] = n -1;

    // REcorre el arreglo por cada elemento de este
    while (TOPE >= 0){
        
        // Se replantean/actualizan lso valores dependiendo del tope
        INI = PILAMENOR[TOPE];
        FIN = PILAMAYOR[TOPE];
        TOPE = TOPE - 1;
        
        // Inspirado en metodo reduce para discriminar si es mayor o menor
        IZQ = INI; 
        DER = FIN;  
        POS = INI; 
        BAND = true;

        while(BAND == true){
            while((vector[POS] <= vector[DER]) && (POS != DER) ){
                DER = DER - 1;
            }

            if(POS == DER){
                BAND = false;
            }

            else{
                AUX = vector[POS];
                vector[POS] = vector[DER];
                vector[DER] = AUX;
                POS = DER;

                while((vector[POS] >= vector[IZQ]) && (POS != IZQ) ){
                    IZQ = IZQ + 1;
                }
                
                if(POS == IZQ){
                    BAND = false;
                }
                
                else{
                    AUX = vector[POS];
                    vector[POS] = vector[IZQ];
                    vector[IZQ] = AUX;
                    POS = IZQ;
                }
            }
        
        }
        
    // Actualiza los datos del paso anterior (Reduccion) para seguir con otros elementos
        if(INI < (POS - 1)){
            TOPE = TOPE + 1;
            PILAMENOR[TOPE] = INI;
            PILAMAYOR[TOPE] = POS -1;
        }

        if(FIN > (POS + 1)){
            TOPE = TOPE + 1;
            PILAMENOR[TOPE] = POS +1;
            PILAMAYOR[TOPE] = FIN;
        }
    }
    
}
