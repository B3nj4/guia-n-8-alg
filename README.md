# Guia Nº8 - Alg

#Algoritmos para el ordenamiento de elementos de un vector

El proyecto trata de ordenar los elemtos de un vector utilizando el algoritmo de "Quicksort" y de "Seleccion ". 

#Pre-requisitos

Para poder instalar el sofware se necesita de un sistema operativo Linux como lo es Ubuntu (link de la instalacion https://ubuntu.com/download)

Es fundamental la instalacion del lenguaje C++ y sus componentes de compilacion en su version g++ 9.3.0 (vease el siguiente link https://hetpro-store.com/TUTORIALES/compilar-cpp-g-linux-en-terminal-leccion-1/)

Se necesitara la instalacion de un editor de texto/IDE como lo puede ser "Visual Studio Code". (vease el link para la instalacion https://ubunlog.com/visual-studio-code-editor-codigo-abierto-ubuntu-20-04/?utm_source=feedburner&utm_medium=%24%7Bfeed%2C+email%7D&utm_campaign=Feed%3A+%24%7BUbunlog%7D+%28%24%7BUbunlog%7D%29)

Por ultimo, para poder clonar el repositorio del sofware, es necesario tener instalado "Gitlab" correctamente.


#Comenzando  

Para poder acceder a este sofware se requerira de clonar el espacio de trabajo desde https://gitlab.com/B3nj4/guia-n-8-alg la siguiente combinacion en su terminal Linux

    git clone https://gitlab.com/B3nj4/guia-n-8-alg.git

Luego, se debera ejecutar el archivo "Makefile" con la finalidad de poder compilar nuestro programa. Para ello debera ingresar en su terminal Linux el siguiente comando

    make

Ya ejecutado el "Makefile" debera ejecutar el programa "Programa.cpp" para poder interactuar con el sofware, para ello es fundamental ingresar lo siguiente en su terminal Linux

    ./Programa (Ingrese tamaño del vector con elementos aleatorios(int)) (Ingrese 1(Si)para mostrar los elementos de los vectores y 2(No) para no mostrarlos)

Por ejemplo, para obtener un vector de 5 elementos y que no me imprima los vectores debemos hacer lo siguiente

    ./Programa 5 2

Debera aparecer esto en pantalla

    -----------------------------------------
    Método           | Tiempo                 
    -----------------------------------------
    Seleccion        | 0.002 milisegundos         
    Quicksort        | 0.001 milisegundos         
    -----------------------------------------

En caso de querer mostrar los datos de los vectores bastara con ingresar un 1 despues del ingreso del tamaño del vector

    ./Programa 5 1

Esto se imprimira

    <Uso de Quicksort> 
    Vector Original: 12  34  11  22  21  
    Vector Reordenado: 11  12  21  22  34  

    <Uso de Seleccion> 
    Vector Original: 12  34  11  22  21  
    Vector Reordenado: 11  12  21  22  34  

    -----------------------------------------
    Método           | Tiempo                 
    -----------------------------------------
    Seleccion        | 0.002 milisegundos         
    Quicksort        | 0.001 milisegundos         
    -----------------------------------------


#Construido con

Para la creacion de este proyecto se necesito del editor de texto Visual Studio Code y el lenguaje de programacion "C++" 


#Autor
    
    Benjamin Vera Garrido - Ingenieria Civil en Bioinformatica


